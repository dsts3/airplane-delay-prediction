# Airplane Delay Prediction

## Table of Contents
1. [Description](#description)
2. [Files in the Repository](#files-in-the-repository)
3. [How to Run](#how-to-run)
4. [Dataset](#dataset)
5. [Methodology](#methodology)
6. [Results](#model-evaluation)
7. [Improvements](#improvements)
8. [Links](#links)
9. [Conclusions](#conclusions)

## Description

This repository contains code for predicting airplane delays. The model is built using logistic regression and utilizes features such as weather conditions, holidays, and historical flight data to make predictions. The goal is to improve customer experience by informing them of potential flight delays due to weather.

## Files in the Repository

- `onpremises.ipynb`: Notebook which contians Python script to do data preprocessing, model building, and evaluation code.
- `requirements.txt`: Contains all the required libraries 
- `README.md`: Describes the code, how to run it and what the user expect if got the code running.

## How to Run

1. Clone this repository and install the required packages using `pip`:
    ```json
        git clone https://gitlab.com/dsts3/airplane-delay-prediction/
     ```  
    Ensure that you have all the necessary datasets available in the same directory as the code.
2. Make sure you have the required libraries installed using `pip install requirements.txt` file
3. Run the cells to get the code running

## Dataset

The dataset consists of domestic flight performance from 2014 to 2018, provided by the Bureau of Transportation Statistics (BTS). It includes scheduled and actual departure and arrival times, along with other flight details. Data are in 60 ZIP files, each containing a CSV file per month over five years.

- Data Source:
  - [BTS Airline On-Time Performance Data](https://www.transtats.bts.gov/Fields.asp?gnoyr_VQ=FGJ)
  - [Weather Data](https://www.ncei.noaa.gov/access/services/data/v1?dataset=daily-summaries&stations=USW00023174,USW00012960,USW00003017,USW00094846,USW00013874,USW00023234,USW00003927,USW00023183,USW00013881&dataTypes=AWND,PRCP,SNOW,SNWD,TAVG,TMIN,TMAX&startDate=2014-01-01&endDate=2018-12-31)

## Methodology
 - Data Collection: Data is collected from the provided ZIP files.
 - Data Preprocessing: We process the data to handle missing values, filter relevant features, and reduce dimensionality for the busiest US airports and top airlines.
 - Exploratory Data Analysis (EDA): Conducted to understand data distributions and feature correlations.
 - Modeling: A baseline classification model is established and iteratively improved.
 - Evaluation: Model performance is evaluated using accuracy, precision, recall, and specificity.

## Model Evaluation
 
The model was evaluated on several metrics to determine its performance. The results are as follows:

### Test
| Metric     | Result  |
| --------   | ------- |
| Accuracy   | 0.7923  |
| Precision  | 0.5524  |
| Recall     | 0.0576  |
| Specificity| 0.9876  |

### Train
| Metric     | Result  |
| --------   | ------- |
| Accuracy   | 0.7925  |
| Precision  | 0.5534  |
| Recall     | 0.0593 |
| Specificity| 0.9873  |

## Improvements

The model has improved in a number of ways through iterative modeling and assessment. These include fine-tuning the logistic regression model to better match the dataset and using feature engineering to emphasize the effect of weather on delays. In order to make sure that our model is reliable and effectively generalizes to new data, we have also included cross-validation.

## Links

 - GitHub: https://gitlab.com/dsts3/airplane-delay-prediction/
 - Tableau: https://public.tableau.com/app/profile/elizabeth.mathachan/viz/EDA-PART2/Airplanedelayprediction?publish=yes

## Conclusions

In conclusion, the forecasting abilities for flight delays have improved through feature development and model experimentation; nonetheless, the recall suggests further refining is necessary to completely achieve business objectives. Future tactics might involve investigating more sophisticated models like Random Forests or Gradient Boosting, utilizing historical delay patterns and peak travel periods, and going deeper into feature engineering. Managing class disparity has proven to be an extremely difficult task, necessitating meticulous feature engineering and selection. The project has demonstrated how crucial it is to undertake extensive data preprocessing, adjust hyperparameters, and use a variety of assessment metrics in order to obtain a complete picture of model performance and make sure it is in line with the business's overall objectives.

